# Docstring:
"""
Initial Alignment Guardian: Beam Splitter/Michelson Alignment.

This guardian script is designed to align the short arm Michelson. It is 
intended to be run after X and Y arm initial alignment, and IR X arm initial 
alignment.

Author: Nathan Holland, A. Mullavey.
Date: 2019-05-13
Contact: nathan.holland@ligo.org

Modified: 2019-05-13 (Created).
Modified: 2019-05-16 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-21 (Some clean up, removing path changes).
Modified: 2019-05-22 (Changed structure to make it more self contained).
Modified: 2019-05-24 (Syntatic changes, debugging from lower level API).
Modified: 2019-05-31 (Added INIT state).
Modified: 2019-06-07 (Fixed ezca feedthrough for align_restore, and align_save
                      functions; Moved towards PEP8 style).
Modified: 2019-06-10 (Moved towards PEP8 style).
Modified: 2019-06-12 (Added notifications).
Modified: 2019-06-24 (Added smarter stopping conditions, based of off initial
                      alignment script data.).
Modified: 2019-07-01 (Added stop conditions into SHORT_ARM_MICH_LOCKED).
Modified: 2019-07-02 (Testing and debugging).
Modified: 2019-07-09 (Fixed data logging error).
"""


#------------------------------------------------------------------------------
# Imports:

# Import the minimum number of needed guardian subutilities.
from guardian import GuardState, GuardStateDecorator, Node

# Import the class Optic from optic.py
# Source @
# /opt/rtcds/userapps/release/asc/l1/guardian/optic.py
from optic import Optic

# Import the classes DigitalDemodulation, and AlignmentDither
# Source @
# /opt/rtcds/userapps/release/asc/l1/scripts/align_dither.py
from align_dither import DigitalDemodulation, AlignmentDither

# Import the LSC matrices access.
# Source @
# /opt/rtcds/userapps/release/isc/l1/guardian/isclib/matrices.py
import isclib.matrices as matrix

# Import the function align_restore from new_align_restore.py.
# Source @
# /opt/rtcds/userapps/release/sus/common/scripts/new_align_restore.py
from new_align_restore import align_restore

# Import the function align_save from new_align_save.py.
# Source @
# /opt/rtcds/userapps/release/sus/common/scripts/align_save.py
from new_align_save import align_save

# Import the class Tracker from
# /opt/rtcds/userapps/release/asc/l1/guardian/gradient_tracker.py
from gradient_tracker import Tracker

# Import the class RunningAvg
# Source @
# /opt/rtcds/userapps/release/als/l1/guardian/runningAvg.py
from runningAvg import RunningAvg

# Import the EzAvg class from
# /opt/rtcds/userapps/release/isc/l1/guardian/isclib/epics_averaging.py
# REPLACE RunningAvg above (which really isn't a running average)
from isclib.epics_average import EzAvg, ezavg

# Import time, to enforce the EPICS refresh rate.
import time
import gpstime

import cdsutils

#------------------------------------------------------------------------------
# Script Variables:

# Nominal state for this guardian.
nominal = "BS_ALIGN_IDLE"
request = nominal

# Convenient access for QUADs and HSTSs
itmx = Optic("ITMX")
etmx = Optic("ETMX")
itmy = Optic("ITMY")
etmy = Optic("ETMY")
prm = Optic("PRM")
srm = Optic("SRM")

#------------------------------------------------------------------------------
# Nodes to manage
imc_node = Node("IMC_LOCK")
alsx_node = Node("ALS_XARM")
alsy_node = Node("ALS_YARM")
bs_node = Node("SUS_BS")

#------------------------------------------------------------------------------
# Functions

def IMC_10W():
    return (imc_node.arrived and (ezca['IMC-IM4_TRANS_SUM_OUTPUT']>9) and (imc_node.state == 'LOCKED_10W'))

#------------------------------------------------------------------------------
# Guardian Decorators

class assert_imc_10W(GuardStateDecorator):
    def pre_exec(self):
        if not IMC_10W():
            return 'SET_POWER_TO_10W'

#------------------------------------------------------------------------------
# Helper Classes.
'''
# A general class for MICH locking.
class MICHlocked(GuardStateDecorator):
    """
    CURRENTLY NOT WORKING: 2019-07-01.
    
    A decorator class designed to enforce that MICH is locked.
    
    It specialises GuardStateDecorator to allow it to accept several arguments
    specifically:
     > Error signal level (Maximum allowable).
     > Reflected power level (Minimum allowable).
     > Reflected power gradient (Maximum allowable).
     > Transmitted power level (Maximum allowable).
     > Transmitted power gradient (Maximum allowable).
    """
    
    # Initialisation function.
    def __init__(self, func, ERR = None, REFL = None, REFL_grad = None,
                 TRANS = None, TRANS_grad = None):
        """
        Initialise the instance of the MICHLocked class. It allows you to
        define the following, beyond the basic GuardState:
         > Error signal level.
         > Reflected power level.
         > Reflected power gradient.
         > Transmitted power level.
         > Transmitted power gradient.
        
        Usage:
        @MICHlocked(ERR, REFL, REFL_grad, TRANS, TRANS_grad)
        guardstate_method(self)
        
        guardstate_method - Method of class, inheriting from GuardState, to
                            decorate.
        ERR               - Optional maximum, absolute, value of the MICH error
                            signal. Default is None, in which case this is
                            ignored.
        REFL              - Optional minimum value of the MICH reflected
                            signal. Default is None, in which case this is
                            ignored.
        REFL_grad         - Optional maximum, absolute, value of the gradient
                            of the MICH reflected signal. Default is None, in
                            which case this is ignored.
        TRANS             - Optional maximum value of the MICH transmitted
                            signal. Default is None, in which case this is
                            ignored.
        TRANS_grad        - Optional maximum, absolute, value of the gradient
                            of the MICH transmitted signal. Default in None, in
                            which case this is ignored.
        """
        # Initialise the base class - GuardStateDecorator.
        super(MICHlocked, self).__init__(func)
        
        
        # Assign the maximum error level.
        self.ERR = ERR
        
        # Assign the minimum reflected level.
        self.REFL = REFL
        
        # Assign the maximum reflected gradient.
        self.REFL_grad = REFL_grad
        
        # Assign the maximum transmitted level.
        self.TRANS = TRANS
        
        # Assign the maximum transmitted gradient.
        self.TRANS_grad = TRANS_grad
        
        
        # Assign the error message list.
        self.err_lst = []
        
        # Assign the flag list.
        self.flag_lst = []
    
    
    # Check the error condition.
    def check_ERR(self):
        """
        Checks that the error condition is met:
        
        |L1:LSC-ASVAC_A_RF45_Q_MON| <= self.ERR
        
        In future I probably want to ensure that the error signal doesn't
        SUBSTANTIALLY PEAK above a certain value.
        """
        # Check that ERR is defined.
        if self.ERR is None:
            # No need to check the error signal.
            self.flag_lst.append(True)
        # Check that the error is less than set value.
        elif self.ERR >= abs(ezca.read(":LSC-ASVAC_A_RF45_Q_MON")):
            # Condition is met.
            self.flag_lst.append(True)
        else:
            # Condition is not met.
            self.flag_lst.append(False)
            
            # Add to the error message.
            self.err_lst.append("|L1:LSC-ASVAC_A_RF45_Q_MON| > " + \
                                "{0:.3e}".format(self.ERR))
    
    
    # Check the reflected condition.
    def check_REFL(self):
        """
        Checks that reflected condition is met:
        
        L1:LSC-POP_A_LF_OUTPUT >= self.REFL
        
        In future I probably want to ensure that the reflected signal
        AVERAGES about a certain value.
        """
        # Check that REFL is defined.
        if self.REFL is None:
            # No need to check reflected signal.
            self.flag_lst.append(True)
        # Check that the reflected is greater than a certain value.
        elif self.REFL <= ezca.read(":LSC-POP_A_LF_OUTPUT"):
            # Condition is met.
            self.flag_lst.append(True)
        else:
            # Condition is not met.
            self.flag_lst.append(False)
            
            # Add to the error message.
            self.err_lst.append("L1:LSC-POP_A_LF_OUTPUT < " + \
                                "{0:.3e}".format(self.REFL))
    
    
    # Check the reflected gradient condition.
    def check_REFL_grad(self):
        """
        Checks that the reflected gradient condition is met:
        
        |d / dt [L1:LSC-POP_A_LF_OUTPUT]| <= self.REFL_grad
        
        Do I need to use this?
        If used it should probably ensure that the AVERAGE GRADIENT is
        approximately zero.
        """
        # Check that REFL_grad is defined.
        if self.REFL_grad is None:
            # No need to check the reflected gradient.
            self.flag_lst.append(True)
        # Check the gradient.
        else:
            # Assign the gradient tracker.
            refl_grad = Tracker()
            
            
            # Set the first value.
            refl_grad.value = ezca.read(":LSC-POP_A_LF_OUTPUT")
            
            # Wait for EPICS refresh.
            time.sleep(0.0625)
            
            # Set the next value.
            refl_grad.value = ezca.read(":LSC-POP_A_LF_OUTPUT")
            
            
            # Check that the gradient is less than set value.
            if self.REFL_grad >= abs(refl_grad.gradient):
                # Condition is met.
                self.flag_lst.append(True)
            else:
                # Condition is not met.
                self.flag_lst.append(False)
                
                # Add to the error message.
                self.err_lst.append("instantaneous backwards derivative of" + \
                                    "L1:LSC-POP_A_LF_OUTPUT > " + \
                                    "{0:.3e}".format(self.REFL_grad))
    
    
    # Check the transmitted condition.
    def check_TRANS(self):
        """
        Checks that the transmitted condition is met:
        
        L1:ASC-AS_C_SUM_OUTPUT <= self.TRANS
        
        In future I probably want to ensure that the transmitted signal
        AVERAGES about a certain value.
        """
        # Check that TRANS is defined.
        if self.TRANS is None:
            # No need to check the transmitted signal.
            self.flag_lst.append(True)
        # Check that the transmitted is less than set value.
        elif self.TRANS >= ezca.read(":ASC-AS_C_SUM_OUTPUT"):
            # Condition is met.
            self.flag_lst.append(True)
        else:
            #Condition is not met.
            self.flag_lst.append(False)
            
            # Add to the error message.
            self.err_lst.append("L1:ASC-AS_C_SUM_OUTPUT < " + \
                                "{0:.3e}".format(self.TRANS))
    
    
    # Check the transmitted gradient condition.
    def check_TRANS_grad(self):
        """
        Checks that the transmitted gradient condition is met:
        
        |d / dt [L1:ASC-AS_C_SUM_OUTPUT]| <= self.TRANS_grad
        
        Do I need to use this?
        If used it should probably ensure that the AVERAGE GRADIENT is
        approximately zero.
        """
        # Check that TRANS_grad is defined.
        if self.TRANS_grad is None:
            # No need to check the reflected gradient.
            self.flag_lst.append(True)
        # Check the gradient.
        else:
            # Assign the gradient tracker.
            trans_grad = Tracker()
            
            
            # Set the first value.
            trans_grad.value = ezca.read(":ASC-AS_C_SUM_OUTPUT")
            
            # Wait for EPICS refresh.
            time.sleep(0.0625)
            
            # Set the next value.
            trans_grad.value = ezca.read(":ASC-AS_C_SUM_OUTPUT")
            
            
            # Check that the gradient is less than set value.
            if self.TRANS_grad >= abs(trans_grad.gradient):
                # Condition is met.
                self.flag_lst.append(True)
            else:
                # Condition is not met.
                self.flag_lst.append(False)
                
                # Add to the error message.
                self.err_lst.append("instantaneous backwards derivative of" + \
                                    "L1:ASC-AS_C_SUM_OUTPUT > " + \
                                    "{0:.3e}".format(self.TRANS_grad))
    
    
    # Reset the error message.
    def reset(self):
        """
        Resets the error message and flag lists.
        Not sure if necessary but better safe than sorry.
        """
        # Reset the error list.
        self.err_lst = []
        
        
        # Reset the flag list.
        self.flag_lst = []
    
    
    # The pre-execution function.
        def pre_exec(self):
            """
            Check the conditions before calling the guardstate method. These
            conditions are defined by what is defined, not None, out of ERR,
            REFL, REFL_grad, TRANS, and TRANS_grad.
            """
            # Reset the errors and flag, lists.
            # Unsure if this step is necessary.
            self.reset()
            
            
            # Check the error condition.
            self.check_ERR()
            
            # Check the reflected condition.
            self.check_REFL()
            
            # Check the reflected gradient condition.
            self.check_REFL_grad()
            
            # Check the transmitted condition.
            self.check_TRANS()
            
            # Check the transmitted gradient.
            self.check_TRANS_grad()

            # Check for failure of previous checks.
            if not all(self.flag_lst):
                # Create the error message.
                err_msg = "Unmet conditions: "
                err_msg += ", ".join(self.err_lst)
                err_msg += '.'
                
                # Notify the operator of the unmet conditions.
                log(err_msg)
                notify(err_msg)
                
                
                # MICH is not locked so fail to the fail safe state.
                # Deliberately not yet implemented - for testing
                # purposes.
                #return "BS_ALIGN_IDLE"
            else:
                # All conditions are met so do nothing.
                pass
'''

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
#                                 Guardian States
#------------------------------------------------------------------------------
# Initial state - Always defined, or added if missing.
class INIT(GuardState):
    request = False
    index = 0
    
    # Main class method - Does nothing.
    def main(self):
        # Succeeds.
        return True


#------------------------------------------------------------------------------
# Idle state - Intentionally does nothing allowing for recovery.
class BS_ALIGN_IDLE(GuardState):
    request = True
    index = 2
    
    
    # Main class method - Sets a timer for 1 second
    def main(self):
        # Set a timer for 1 s.
        self.timer["wait"] = 1
    
    
    # Run class method - Waits for the timer to expire.
    def run(self):
        # Check for timer expiration.
        if self.timer["wait"]:
            # Succeed.
            return True


#------------------------------------------------------------------------------
# Reset state - Reset all of the changes to filters matrices and optics
# done in this guardian.
class BS_ALIGN_RESET(GuardState):
    goto = True
    request = True
    index = 1
    
    # Main class mathod - Reset.
    def main(self):
        # TODO: Unlock MICH first.
        
        # Reset alignment of ETMs
        #etmx.align('P', ezca)
        #etmy.align('P', ezca)
        
        # Reset alignment of PRM.
        #prm.align('P', ezca)
        #prm.align('Y', ezca)
        
        #Reset alignment of SRM.
        #srm.align('P', ezca)
        
        # Reset alignment of ITMs
        #itmx.align('P', ezca)
        #itmx.align('Y', ezca)
        #itmy.align('P', ezca)
        #itmy.align('Y', ezca)
        
        # Reset alignment of the BS.
        # align_restore("BS", ezca)
        
        # Close the fast shutter. FIXME: Don't think this is necessary
        #ezca.write(":SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK", 0)
        
        
        # Reset the BS OSEMs BIO acquire state. FIXME: Not the acquire state
        #for osem in ["UL", "UR", "LL", "LR"]:
        #    #Reset BIO acquire state, values gleaned from looking back
        #    # in time.
        #    ezca.write(":SUS-BS_BIO_M2_{0}_STATEREQ".format(osem), 3)
        
        
        # Reset the LSC input matrix, according to ISC_LOCK (DOWN).
        matrix.lsc_input.zero(row = "MICH")
        matrix.lsc_input["MICH", "REFL_A_RF45_Q"] = 1
        matrix.lsc_input.load()
        
        # Reset the triggering
        ezca.write(":LSC-MICH_TRIG_THRESH_ON", 800)
        matrix.lsc_trigger["MICH", "POPAIR_B_RF18_I"] = 1

        # Reset filter triggering.
        ezca.write(":LSC-MICH_MASK_FM2", 1)
        ezca.write(":LSC-MICH_MASK_FM8", 1)
        
        # Turn off the integrators.
        ezca.switch(":LSC-MICH", "FM2", "FM5", "OFF")
        
        # Reset the AS camera exposure.
        ezca.write(":CAM-AS_EXP", 600)

        # Reset MICH filter module.
        ezca.write(":LSC-MICH_GAIN", 0)
        ezca.write(":LSC-MICH_TRAMP", 1)

        # Unfreeze MICH.
        ezca.write(":LSC-CPSFF_GAIN", 0)

        # Turn off the BS pitch oscillator.
        ezca.write(":ASC-ADS_PIT3_OSC_TRAMP", 0)
        ezca.write(":ASC-ADS_LO_PIT_MTRX_4_3", 0)
        ezca.write(":ASC-ADS_PIT3_OSC_CLKGAIN", 0)
        
        # Turn off the BS yaw oscillator.
        ezca.write(":ASC-ADS_YAW3_OSC_TRAMP", 0)
        ezca.write(":ASC-ADS_LO_YAW_MTRX_4_3", 0)
        ezca.write(":ASC-ADS_YAW3_OSC_CLKGAIN", 0)

        # Set a 5 second timer, for changes to take effect.
        self.timer["wait"] = 5

    # Run class method - wait for timer to expire.
    def run(self):
        # Check timer expiration.
        if self.timer["wait"]:
            # Succeed.
            return True

#------------------------------------------------------------------------------
# Misalignment state - Misalgin optics so that only MICH is resonant.
class ALIGN_FOR_MICH(GuardState):
    request = False
    index = 4
    
    # Main class method - Misalign optics.
    def main(self):

        # ALS guardians should not try to lock here
        alsx_node.set_request("QPDS_LOCKED")
        alsy_node.set_request("QPDS_LOCKED")

        # Misalign the RMs.
        prm.misalign('P', 1500, ezca)
        prm.misalign('Y', 1500, ezca)
        srm.misalign('P', -1500, ezca)
        itmx.align('P',ezca)
        itmx.align('Y',ezca)
        itmy.align('P',ezca)
        itmy.align('Y',ezca)
        
        # Misalign ETMs.
        etmx.misalign('P', -40, ezca)
        etmy.misalign('P', -40, ezca)

    def run(self):

        return True

class SKIP_ALIGN_FOR_MICH(GuardState):
    request = False
    index = 3

    def run(self):

        return True

#------------------------------------------------------------------------------
# Prep state - BS suspension setting, dark offsets 
class PREPARE_FOR_MICH(GuardState):
    request = True
    index = 5
    
    # Main class method - Misalign optics.
    def main(self):
        
        # Put the BS OSEMs in acquire state.
        for osem in ["UL", "UR", "LL", "LR"]:
            # Acquire state is state 2.
            ezca.write(":SUS-BS_BIO_M2_{0}_STATEREQ".format(osem), 2)
        #bs_node.set_request("COILDRIVER_LOCK_ACQ")

        # Set the AS camera exposure.
        ezca.write(":CAM-AS_EXP", 6000)

        # Enable the BS OPLEV.
        ezca.switch(":SUS-BS_M2_OLDAMP_P", "OUTPUT", "ON")

        # Set a timer, to ensure changes take effect.
        self.timer["wait"] = 3

        # Block fast shutter to set dark offsets
        ezca.write(":SYS-MOTION_C_FASTSHUTTER_A_BLOCK", 1)
        time.sleep(2)

        chans = []
        for phase in ['I','Q']:
            for seg in ['1','2','3','4']:
                chans.append(':ASC-AS_A_RF45_{}{}_INMON'.format(phase,seg))

        dark_offsets = ezavg(ezca,5,chans)

        ii = 0
        for phase in ['I','Q']:
            for seg in ['1','2','3','4']:
                ezca.write(':ASC-AS_A_RF45_{}{}_OFFSET'.format(phase,seg),-1.0*dark_offsets[ii])
                ii+=1

    # Run class method - Wait for the changes to take effect.
    def run(self):
        # Check for timer expiration.
        if self.timer["wait"]:
            # Succeed once timer has expired.
            return True

#------------------------------------------------------------------------------
class SET_POWER_TO_10W(GuardState):
    request = False
    index = 7

    def main(self):
        
        # Need to do this to steal control?
        imc_node.set_managed()
        # Request 10W (maybe better to use IFO POWER state?)
        imc_node.set_request("LOCKED_10W")
        # Release control, no need to micromanage
        imc_node.release()

        # Set a timer to periodically check IMC status 
        self.timer['check'] = 2

        # A timer to tell the operator that IMC is taking too long
        self.timer['timeout'] = 120

    def run(self):

        if self.timer['timeout']:
            notify("IMC_LOCKED guardian has failed to reach \"LOCKED_10W\" "+ \
                   "within 120 seconds (timeout), please investigate.")

        if not self.timer['check']:
            return

        # Check every two seconds
        if not IMC_10W():
            self.timer['check'] = 2
            return

        return True

#------------------------------------------------------------------------------
# Lock Acquisition state - Lock MICH.
class LOCK_SHORT_ARM_MICH(GuardState):
    request = False
    index = 9

    @assert_imc_10W
    def main(self):
        # Unshutter.
        ezca.write(":SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK", 1)


        # Freeze BS
        ezca['LSC-CPSFF_GAIN'] = 10.0 #8.3

        # Change the LSC input matrix.
        matrix.lsc_input.zero(row="MICH")
        matrix.lsc_input["MICH", "ASVAC_A_RF45_Q"] = 1
        matrix.lsc_input.TRAMP = 0
        matrix.lsc_input.load()

        # Remove MICH triggering.
        ezca.write(":LSC-MICH_TRIG_THRESH_ON", -1)
        ezca.write(":LSC-TRIG_MTRX_2_2", 0)
        
        # Remove MICH filter triggering.
        ezca.write(":LSC-MICH_MASK_FM2", 0)
        ezca.write(":LSC-MICH_MASK_FM6", 0)
        ezca.write(":LSC-MICH_MASK_FM7", 0)
        ezca.write(":LSC-MICH_MASK_FM8", 0)

        # Get the LSC MICH filter.
        self.mich_filter = ezca.get_LIGOFilter(":LSC-MICH")
        
        # Ramp the MICH gain, waiting until completion.
        self.mich_filter.ramp_gain(0.0, ramp_time = 0.1, wait = False)
        
        # Ensure that only the following are enabled.
        # What about limits and offsets?
        self.mich_filter.only_on("INPUT", "FM1", "FM3", "FM6", "FM7", "OUTPUT")
        
        # Reset the MICH filter history.
        ezca.write(":LSC-MICH_RSET", 2)
        
        # Ramp the MICH gain up again, do not wait.
        # Might fail.
        self.mich_filter.ramp_gain(3000, ramp_time = 10, wait = False)

        # Chans to avg
        chans = ['LSC-ASVAC_A_RF45_Q_MON','LSC-POP_A_LF_OUTPUT','ASC-AS_C_SUM_OUTPUT']
        self.michAvgs = EzAvg(ezca,3.0,chans)

        # Flags for checking the completion conditions.
        self.flags = [False, False, False]

        # Set a timer, because checking ramp times doesn't work.
        self.timer["ramp"] = 10

    @assert_imc_10W
    def run(self):
        # Check if the filter is still ramping.
        if not self.timer["ramp"]:
            # Do not perform any checks until gain ramping is done.
            notify("Locking Short Arm Michelson.")
            return False

        # TODO: Improve the end condition monitoring, such asf adding
        # averages.

        #error = cdsutils.avg(-3,'LSC-ASVAC_A_RF45_Q_MON')
        #refl = cdsutils.avg(-3,'LSC-POP_A_LF_OUTPUT')
        #trans = cdsutils.avg(-3, 'ASC-AS_C_SUM_OUTPUT')

        [done,vals] = self.michAvgs.ezAvg()
        if not done:
            notify('Filling EzAvg buffer')
            return

        error = vals[0]
        refl = vals[1]
        trans = vals[2]

        # TODO: This code can be simplified with 'or's
        # Check the error signal condition.
        #if abs(val[0]) < 20:
        if abs(error) < 20:
            self.flags[0] = True
        else:
            self.flags[0] = False
            notify("Error signal is too large.")
        
        # Check that the reflected condition is met.
        #if ezca.read(":LSC-POP_A_LF_OUTPUT") > 0.2:
        if refl > 0.20:			#Changed from 0.25 AJM211109
            self.flags[1] = True
        else:
            self.flags[1] = False
            notify("Reflected signal is too small. Align better.")
        
        # Check that the transmitted signal is met.
        #if ezca.read(":ASC-AS_C_SUM_OUTPUT") < 10:
        if trans < 200:
            self.flags[2] = True
        else:
            self.flags[2] = False
            notify("Transmitted signal is too large. Align better.")
        
        # Check that all conditions are met.
        if all(self.flags):
            # All tests passed.
            ezca.switch('LSC-MICH','FM2','ON')
            return True


#------------------------------------------------------------------------------
# Locked State - The short arm michelson is locked.
class SHORT_ARM_MICH_LOCKED(GuardState):
    request = True
    index = 10
    
    # Main class method - Temporarily here to allow data collection.
    def main(self):
        # Define a flag for the first run iteration.
        self.first_run = True
        
        # Define a timer, to collect data for a fixed period of time.
        self.timer["collect"] = 10

    # Run class method - Wait until lock is finalised and ensure that
    # MICH remains locked.
    def run(self):
        # TODO: Move to using a GuardianStateDecorator to ensure that
        # MICH is locked. 
        
        # TODO: Any error checking here.
        
        # TODO: Succeed when the GuardianStateDecorator, and error.
        # checking succeeds.

        # Check for the first iteration of run.
#        if self.first_run:
#            # Set the first run to False.
#            self.first_run = False
#            
#            
#            # Setup the data header:
#            header = "TRANS, REFL, ERR"
#            log(header)
#        
#        
#        # Collect the data.
#        data = "DATA: {0:.3e}".format(ezca.read(":ASC-AS_C_SUM_OUTPUT")) + \
#               ", {0:.3e}".format(ezca.read(":LSC-POP_A_LF_OUTPUT")) + \
#               ", {0:.3e}".format(ezca.read(":LSC-ASVAC_A_RF45_Q_MON"))
#        
#        # Log the data.
#        log(data)
        
        
        # Check for timer expiration.
        if self.timer["collect"]:
            # Succeed.
            return True


#------------------------------------------------------------------------------

# Checking State - Ensures that the AS port is stable.
class CHECK_AS_STABLE(GuardState):
    request = False
    index = 15
    
    # Main class method - Setup to determine if the AS port is stable.
    def main(self):
        # TODO: Setup.

        # Define a flag for the first run iteration.
        self.first_run = True
        
        # Define a timer, to collect data for a fixed period of time.
        self.timer["collect"] = 10

        # Wait, I want the AS port to be stable for a minimum amount of
        # time. Here I set that as 5 seconds.
        self.timer["min_wait"] = 5

    # Run class method - Check that the AS port is stable.
    def run(self):
        # TODO: Use a GuardStateDecorator to ensure that MICH is
        # locked.

        # Check for minimum timer expiration.
        if not self.timer["min_wait"]:
            # Timer has not expired, so skip the remainder of the
            # steps.
            return False

        # TODO: Any error checking.

        # TODO: Check that the AS port is stable.

#        # Check for the first iteration of run.
#        if self.first_run:
#            # Set the first run to False.
#            self.first_run = False
# 
#            # Setup the data header:
#            header = "TRANS, REFL, ERR"
#
#           log(header)
#
#        # Collect the data.
#        data = "DATA: {0:.3e}".format(ezca.read(":ASC-AS_C_SUM_OUTPUT")) + \
#               ", {0:.3e}".format(ezca.read(":LSC-POP_A_LF_OUTPUT")) + \
#               ", {0:.3e}".format(ezca.read(":LSC-ASVAC_A_RF45_Q_MON"))
#        
#        # Log the data.
#        log(data)
                
        # Check for timer expiration.
        if not self.timer["collect"]:
            # Succeed.
            return

        return True

#------------------------------------------------------------------------------
# Automatic alignment step - Automatically align the BS.
class ALIGN_BS(GuardState):
    request = False
    index = 19
    
    # Main class method - Setup for automatic BS alignment.
    def main(self):
        # Turn on the MICH integrators.
        ezca.switch(":LSC-MICH", "FM2", "FM5", "ON")
                
        # Set the BS optic align ramp times.
        ezca.write(":SUS-BS_M1_OPTICALIGN_Y_TRAMP", 1)
        ezca.write(":SUS-BS_M1_OPTICALIGN_P_TRAMP", 1)
        
        matrix.asc_ads_lo_pit.zero(col='OSC3')
        matrix.asc_ads_lo_yaw.zero(col='OSC3')

        # Turn on the pitch oscillator.
        ezca.write(":ASC-ADS_PIT3_OSC_FREQ", 0.9)
        ezca.write(":ASC-ADS_PIT3_OSC_TRAMP", 2)
        #ezca.write(":ASC-ADS_LO_PIT_MTRX_6_3", 1)
        matrix.asc_ads_lo_pit['BS','OSC3'] = 1
        ezca.write(":ASC-ADS_PIT3_OSC_CLKGAIN", 15)
        
        # Turn on the yaw oscillator.
        ezca.write(":ASC-ADS_YAW3_OSC_FREQ", 1.4)
        ezca.write(":ASC-ADS_YAW3_OSC_TRAMP", 2)
        #ezca.write(":ASC-ADS_LO_YAW_MTRX_6_3", 1)
        matrix.asc_ads_lo_yaw['BS','OSC3'] = 1
        ezca.write(":ASC-ADS_YAW3_OSC_CLKGAIN", 25)
        
        # Assign a digital demodulator to obtain the pitch and yaw
        # error signals by CIC filtering.
        self.demod = DigitalDemodulation(ezca, ":ASC-AS_A_DC_SUM_OUTPUT",
                                         ":SUS-BS_M2_DITHER_P_INMON",
                                         ":SUS-BS_M2_DITHER_Y_INMON")
        #FIXME: Why do we use AS_A here and not AS_C?        

        # Pitch alignment dither servo.
        self.ad_p = AlignmentDither(ezca, ":SUS-BS_M1_OPTICALIGN_P_OFFSET", 
                                    4e-4)
        # Yaw alignment dither servo.
        self.ad_y = AlignmentDither(ezca, ":SUS-BS_M1_OPTICALIGN_Y_OFFSET",
                                    3e-4)
        
        # TODO: Any setup for the better stopping condition.
        
        # Define a flag for the first run iteration.
        self.first_run = True
        
        # Set a timer, which allows the integrators to settle.
        self.timer["settle"] = 1
        
        # Set a run time, timer. For now this sets the stopping
        # condition.
        self.timer["run"] = 40

        # Filename for writing data to
        pathname = '/opt/rtcds/userapps/trunk/asc/l1/guardian/data/'
        filetime = str(int(gpstime.gpsnow()))
        filename = "bs_align_signals"
        self.fname = pathname + filename + '_' + filetime + '.txt'

    # Run class method - Run the automatic alignment.
    def run(self):
        # TODO: Use a GuardStateDecorator to ensure that MICH is
        # locked.
        
        # Check integrator settle timer.
        if not self.timer["settle"]:
            # Integrators have not yet finished settling. Do not
            # continue with the remainder of this method.
            return False
        
        # TODO: Basic error checking.
        
        # Collect and demodulate the data for 2 seconds.
        pwr, err_p, err_y = self.demod.demod()
        
        # Check to see if this is the first run.
        if self.first_run:
            # Set flag to false.
            self.first_run = False
            
            # Log a header for the DATA.
            header = "Gps-Time\tAvg-TRANS\tPit-Err\tYaw-Err\tTrans\tRefl\n"
            with open (self.fname, "a") as f:
                f.write(header)
        
        # Write data to file for analysis
        gtime = gpstime.gpsnow()
        data = "{0:.4f}\t{1:.3e}\t{2:.3e}\t{3:.3e}".format(gtime, pwr, err_p, err_y) + \
               "\t{0:.3e}".format(ezca.read(":ASC-AS_A_DC_SUM_OUTPUT")) + \
               "\t{0:.3e}\n".format(ezca.read(":LSC-POP_A_LF_OUTPUT"))
        with open (self.fname, "a") as f:
            f.write(data)
        
        
        # Check for sufficient power for automatic procedure.
        if pwr <= -1000:
            # Inform the operator that  the power is insufficient for
            # automatic alignment.
            # TODO: Ensure that this error message is useful!
            log("Power on BS too low. Please manually align BS.")
            notify("Power on BS too low. Please manually align BS.")
            
            # Jump to manual alignment state.
            return "MANUAL_BS_ALIGNMENT"
        
        # TODO: Implement, and enforce better stopping condition.
        # Avg AS_A below 10, AS_C below 10, POP above 0.35, 
        # absolute of Errs less than 20 
        
        # Check for run timer expiration.
        if self.timer["run"]:
            # Succeed now that the run timer has expired.
            return True
        
        # Perform one iteration of the pitch alignment servo.
        self.ad_p.align_dither(err_p, 1)
        
        # Perform one iteration of the yaw alignment servo.
        self.ad_y.align_dither(err_y, 1)

#------------------------------------------------------------------------------
# Manual Alignment state - Allow manual alignment of the BS.
class MANUAL_BS_ALIGNMENT(GuardState):
    request = False
    index = 17
    
    # Main class method - Disable oscillators and setup monitor.
    def main(self):
        # Turn off the pitch oscillator.
        ezca.write(":ASC-ADS_PIT3_OSC_TRAMP", 0)
        ezca.write(":ASC-ADS_LO_PIT_MTRX_4_3", 0)
        ezca.write(":ASC-ADS_PIT3_OSC_CLKGAIN", 0)
        
        # Turn off the yaw oscillator.
        ezca.write(":ASC-ADS_YAW3_OSC_TRAMP", 0)
        ezca.write(":ASC-ADS_LO_YAW_MTRX_4_3", 0)
        ezca.write(":ASC-ADS_YAW3_OSC_CLKGAIN", 0)
        
        
        # Set up a digital demodulator to monitor the power. Will
        # return to automaict alignment if the power rises
        # sufficiently.
        self.demod = DigitalDemodulation(":ASC-AS_A_DC_SUM_OUTPUT",
                                         ":SUS-BS_M2_DITHER_P_INMON",
                                         ":SUS-BS_M2_DITHER_Y_INMON")
        
        # Set timer to remind the operator that it is in manual
        # alignment mode. Reminds every 15 seconds
        self.timer["remind"] = 15
    
    # Run class method.
    def run(self):
        # TODO: Use a GuardStateDecorator to ensure that MICH is
        # locked.
        
        # Check for the reminder timer expiration.
        if self.timer["remind"]:
            # Remind the operator.
            log("Please manually align BS.")
            notify("Please manually align BS.")

            # Reset the reminder timer.
            self.timer["remind"] = 15

        # TODO: Error checking, e.g. is MICH still locked?

        # Get the average power, and discard the demodulations.
        pwr, _ = self.demod.demod()
        
        # Check that the power is sufficient for automatic alignment.
        if pwr > -1000:
            # Warn the operator that automatic alignment is resuming.
            log("BS power is now sufficient. Resuming automatic alignment.")
            notify("BS power is now sufficient. Resuming automatic alignment.")

            # Return to the automatic alignment state.
            return True

        # TODO: In future some fancy automatic scan procedure.

        # Remain in the manual alignment state. 
        return False

#------------------------------------------------------------------------------

# Aligned state -  The final, aligned state, for the BS.
class BS_ALIGNED(GuardState):
    request = True
    index = 20
    
    # Main class method - Disable oscillator and save the BS position.
    def main(self):
        # Turn off the pitch oscillator.
        ezca.write(":ASC-ADS_PIT3_OSC_TRAMP", 0)
        ezca.write(":ASC-ADS_LO_PIT_MTRX_4_3", 0)
        ezca.write(":ASC-ADS_PIT3_OSC_CLKGAIN", 0)
        
        # Turn off the yaw oscillator.
        ezca.write(":ASC-ADS_YAW3_OSC_TRAMP", 0)
        ezca.write(":ASC-ADS_LO_YAW_MTRX_4_3", 0)
        ezca.write(":ASC-ADS_YAW3_OSC_CLKGAIN", 0)
        
        # Save the position of the BS.
        align_save("BS", ezca)
        
        # TODO: Setup to monitor that the BS is still aligned.
        
        # Define a flag for the first run iteration.
        self.first_run = True
        
        # Define a timer for data collection.
        self.timer["collect"] = 10
    
    
    #Run class method - Do nothing, currently.
    def run(self):
        # TODO: Use a GuardStateDecorator to ensure MICH is locked.
        
        # TODO: Error checking, e.g. is MICH still locked.
        
        
        # TODO: Ensure that the BS is still aligned. Likely a monitor
        # on power and error signal levels.
        
        
        # Check to see if it is the first iteration.
#        if self.first_run:
#            # Set the first run flag to false.
#            self.first_run = False
#            
#            # Log a header for the data collection.
#            header = "TRANS, REFL, ERR"
#            log(header)
#        
#        # Collect and log the data.
#        data = "DATA: {0:.3e}".format(ezca.read(":ASC-AS_A_DC_SUM_OUTPUT")) + \
#               ", {0:.3e}".format(ezca.read(":LSC-POP_A_LF_OUTPUT")) + \
#               ", {0:.3e}".format(ezca.read(":LSC-ASVAC_A_RF45_Q_MON"))
#        
#        # Check for running collection timer.
#        if not self.timer["collect"]:
#            # Log the data.
#            log(data)
        
        
        # Succeed.
        return True


#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
#                                    Edges
#------------------------------------------------------------------------------

# Allowable transitions.
edges = [
         # Recovery.
         ("INIT", "BS_ALIGN_IDLE"),
         ("BS_ALIGN_RESET", "BS_ALIGN_IDLE"),
         # Main procedure.
         ("BS_ALIGN_IDLE","ALIGN_FOR_MICH"), 
         ("ALIGN_FOR_MICH","PREPARE_FOR_MICH"),
         ("BS_ALIGN_IDLE","SKIP_ALIGN_FOR_MICH",2),
         ("SKIP_ALIGN_FOR_MICH","PREPARE_FOR_MICH"),
         ("PREPARE_FOR_MICH", "LOCK_SHORT_ARM_MICH"),
         ("SET_POWER_TO_10W","LOCK_SHORT_ARM_MICH"),
         ("LOCK_SHORT_ARM_MICH", "SHORT_ARM_MICH_LOCKED"),
         ("SHORT_ARM_MICH_LOCKED", "CHECK_AS_STABLE"),
         ("CHECK_AS_STABLE", "ALIGN_BS"),
         ("ALIGN_BS", "MANUAL_BS_ALIGNMENT"),
         ("MANUAL_BS_ALIGNMENT", "ALIGN_BS"),
         ("ALIGN_BS", "BS_ALIGNED")
        ]


# END.
